# Repte: Usar FTK Imager

## Temps
30 minuts

## Material i programari

- OVA Windows 10 (imatge mínima) - https://www.scivision.dev/free-windows-virtual-machine-images/
- FTK Imager - https://accessdata.com/product-download/ftk-imager-version-4-5
- USB petit

## Objectiu

Fer una còpia bit a bit de tots els sectors d'un disc USB amb una eina com FTK Imager.

**NOTA**: FTK Imager no garanteix que no s'escrigui al disc mentre s'està fent la imatge. Per aquesta raó, cal fer servir un blocador d'escriptura en casos reals. En aquesta activitat assumirem que tenim un blocador d'escriptura USB.

## Exercici

1. Engegar FTK imager i inserir el disc USB.
2. Crear una imatge del disc USB en format cru (Raw dd) i guardar la còpia a una carpeta compartida amb la màquina física.
3. Explorar altres característiques de FTK Imager.

## Preguntes de revisió

1. Després de la còpia, quins fitxers ha creat FTK Imager? Selecciona totes les opcions correctes.
- Fitxer Imatge amb extensió .001
- Fitxers individuals com al disc original
- Fitxer de text amb el sumari de la imatge
- Fitxer Imatge amb extensió .ex001

2. L'opció "Verify images after they are created" està activada per defecte i el seu efecte és:
- FTK imager calcula el hash de la imatge
- FTK imager calcula el hash del disc USB
- FTK imager calcula el hash MD5 i SHA1 del disc USB i de la imatge i comprova que coincideixen.
- FTK imager calcula el hash MD5 del disc USB i el hash MD5 de la imatge, i comprova que coincideixen

3. Quants algoritmes hash usa FTK imager per verificar que la imatge no ha sigut alterada?
- Un algoritmes hash
- Dos algoritmes hash
- Tres algoritmes hash
- Quatre algoritmes hash

4. Un cop carregada la imatge dd en FTK Imager, es veuen els arxius esborrats?
- Si
- No

5. FTK Imager pot fer captures de memòria RAM?
- Cert
- Fals

6. Quins altres formats d'imatge de disc es poden generar amb FTK IMager? Descriu quines diferències hi ha amb la còpia dd "crua" (RAW).


# Respostes

**1.** El FTK Imager ha creat fitxers amb el nom que li he possat a la hora de crear la imatge (Usb) i amb extensió, 001 amb un fitxer de text amb el 
sumari de la imatge.

![Foto7](https://gitlab.com/GiorgiMskhiladze/ciberclassespau/-/raw/main/Act1/Fotos/7.PNG)

**2.** FTK imager calcula el hash MD5 i SHA1 del disc USB i de la imatge i comprova que coincideixen.

**3.** Utilitza 2 algoritmes hash, el MD5 i el SHA1

![Foto4](https://gitlab.com/GiorgiMskhiladze/ciberclassespau/-/raw/main/Act1/Fotos/4.PNG)

**4.** Al carregar la imatge dd, no es veuen els arxius borrats, que eren 2 PDFs, un document Word i un fitxer texte

![Foto5](https://gitlab.com/GiorgiMskhiladze/ciberclassespau/-/raw/main/Act1/Fotos/5.PNG)

**5.** Cert, et fa un dump de la memoria RAM en un fitxer que li diguis

![Foto7](https://gitlab.com/GiorgiMskhiladze/ciberclassespau/-/raw/main/Act1/Fotos/7.PNG)

**6.** Es poden crear també imatges de tipus SMART, E01, AFF. El tipus SMART está disenyat per siste d'arxius Linux, en comptes de copiar bit a bit, fa un fluxe de bits que les pots comprimir de forma opcional.
El E01 és de tipus propietari creada per Guidance Software's EnCase, el que fa es comprimir la imatge,guardar en la capçelera i en el peu de pagina el hash MD5 de tots els fluxes de bits.
I el AFF creada per Simson Garfinkel and Basis Technology, té com a objectiu crear una imatge del disc no propietaria per no forcar al usuari en un format patentat i que aixó li eviti poder analitzar de forma correcta.


### Enllaços

- https://www.forensicfocus.com/articles/evidence-acquisition-using-accessdata-ftk-imager/
